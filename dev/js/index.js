//WOW JS
import { WOW } from 'wowjs/dist/wow.min'

//BaguetteBox
import baguetteBox from 'baguettebox.js/dist/baguetteBox.min'

//Instafeed JS
import Instafeed from 'instafeed.js/instafeed.min'

//Animacion de click Scrooll
import SmoothScroll from "smooth-scroll/dist/js/smooth-scroll.min"

//Data vuejs
import { contactform } from './modules/contactform'
// import { menuinicio, mainmenu } from './modules/menus'

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'

Vue.use(VueResource);

new Vue({
  el: '#app',
  data: {
    showmodal: false,
    toggle: false,
    formSubmitted: false,
    vue: contactform,
    // menuinicio,
    // mainmenu,
    // proveedores
  },
  created: () => {
    let wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: false
    })
    wow.init()

    var scroll = new SmoothScroll('a[href*="#"]', {
      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)

      // Speed & Easing
      speed: 500, // Integer. How fast to complete the scroll in milliseconds
      offset: 118, // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function (time) { }, // Function. Custom easing pattern

      // Callback API
      before: function () { }, // Callback to run before scroll
      after: function () { } // Callback to run after scroll
    });

    //FOOTER AÑO
    function getDate() {
      var today = new Date()
      var year = today.getFullYear()
      document.getElementById('currentDate').innerHTML = year
    }
    getDate()
  },
  mounted: () => {
    baguetteBox.run('.gallery')
    var galleryFeed = new Instafeed({
      get: 'tagged',
      tagName: 'proyectosMRHH',
      clientId: 'b75eb0eacc014fe69f2b3faaa7302888',
      accessToken: "6725736689.b75eb0e.b31e228283fc4618a19e8d8fb8e1d1e5",
      resolution: "standard_resolution",
      // useHttp: "true",
      limit: 4,
      template: '<div class="proyectos__card"><div class= "proyectos__card_content" > <img src="{{image}}" alt=""><div class="proyectos__card_descrip"><p>{{caption}}</p></div></div></div >',
      // '<div class="col33"><a href="{{image}}"><div class="img-featured-container"><div class="img-backdrop"></div><div class="description-container"><p class="caption">{{caption}}</p><span class="likes"><i class="icon ion-heart"></i> {{likes}}</span><span class="comments"><i class="icon ion-chatbubble"></i> {{comments}}</span></div><img src="{{image}}" class="img-responsive"></div></a></div>',
      target: "instafeed-gallery-feed",
      after: function() {
        // disable button if no more results to load
        if (!this.hasNext()) {
          btnInstafeedLoad.setAttribute('style', 'display: none;');
          var instagram = document.createElement("a");
          var t = document.createTextNode("Ver más en Instagram");
          instagram.setAttribute('href', "https://www.instagram.com/mrhandyhand10/");
          instagram.setAttribute('target', "_black");
          instagram.setAttribute('class', "button__initial");
          instagram.appendChild(t);  
          document.getElementById("instagram").appendChild(instagram);  
          getTheTableTag.appendChild(instagram);          
        }
      },
    });
    galleryFeed.run();
    var btnInstafeedLoad = document.getElementById("btn-instafeed-load");
    btnInstafeedLoad.addEventListener("click", function() {
      galleryFeed.next()
    });

  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('/mail.php', { vue: this.vue }).then(function (response) {
        // console.log(response)
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    toggleMenu: function () {
      if (this.toggle == true) {
        this.toggle = false
        console.log('DESACTIVADO')
      }
      else {
        this.toggle = true
        console.log('ACTIVO')
      }
    }
  }
})